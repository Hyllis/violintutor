# ViolinTutor #

ViolinTutor allows you to plug your electric violin to your computer and use it to interactively **play along any sheet music** (MusicXML format). You can also **learn sightreading**, and the name of the **notes on the fingerboard**.

# Demo #

[![ViolinTutor Demo Video](https://hyllis.me/violintutor/video_preview.png)](https://www.youtube.com/watch?v=oS7gncDoz94 "Everything Is AWESOME")

# Usage #

To run the program:

```
python3 run.py
```

You should use a device such as the [Behringer UCG102 Guitar Link][guitarlink] to plug your electric violin to your computer, and use it as your default microphone. An acoustic violin and a good microphone might also work. It's also possible to simply play along, without the interactive features.

# Sheet music #

You need to store your **uncompressed MusicXML** files in the *songs/* folder of the project.

There is thousands of sheets available on [MuseScore][musescore]'s website, but most are compressed; opening the compressed sheet in MuseScore and re-saving it as uncompressed MusicXML is the easiest way (this also allows you to convert MIDI files to MusicXML).

[FlatIO][flatio] is a easy way to create your own MusicXML sheets online.


[guitarlink]: https://www.thomann.de/fr/behringer_ucg102.htm "Behringer UCG102 - Thomann"

[musescore]: https://musescore.com/sheetmusic?text=violin&instruments=9 "MuseScore"

[flatio]: http://flat.io/  "FlatIO"
