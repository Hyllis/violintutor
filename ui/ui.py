import pygame
from logic.input_manager import *

class UI:
	def __init__(self, screen, inputm):
		self.exit = False
		self.inputm = inputm
		self.screen = screen

		self.font24 = pygame.font.Font(None, 24)
		self.font32 = pygame.font.Font(None, 32)
		self.font48 = pygame.font.Font(None, 48)
		self.font54 = pygame.font.Font(None, 54)
		self.font68 = pygame.font.Font(None, 68)
		self.font128 = pygame.font.Font(None, 128)

	def create_button(self, callback, surf, button, scale, pos, label, font, fontcolor, offset=(0,0), **args):
		surf.blit(pygame.transform.scale(button, scale), pos)
		t = font.render(label, 1, fontcolor)
		surf.blit(t, (pos[0] + scale[0]/2 - t.get_width()/2, pos[1] + scale[1]/2 - t.get_height()/2))
		self.inputm.add_mouse_event(callback, (offset[0]+pos[0], offset[1]+pos[1],
			offset[0]+pos[0]+scale[0], offset[1]+pos[1]+scale[1]))

	def load_background(self, filename):
		self.bg = pygame.image.load(filename)
	def blit_background(self):
		self.screen.blit(self.bg, (0,0))

	def flip(self):
		pygame.display.flip()
