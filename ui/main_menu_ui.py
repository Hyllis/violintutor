from ui.ui import *
from pygame import *

class MainMenuUI(UI):
	def load_menu(self, bt1, bt2, bt3, bt4):
		self.menu = pygame.Surface((350, 300), pygame.SRCALPHA)
		self.menupos = (190, self.screen.get_height()/2 - self.menu.get_height()/2)
		self.create_button(bt1[1], self.menu, self.button_wood,
				(350,60), (0,0), bt1[0], self.font32, (255,255,255), self.menupos)
		self.create_button(bt2[1], self.menu, self.button_wood,
				(350,60), (0,80), bt2[0], self.font32, (255,255,255), self.menupos)
		self.create_button(bt3[1], self.menu, self.button_wood,
				(350,60), (0,160), bt3[0], self.font32, (255,255,255), self.menupos)
		self.create_button(bt4[1], self.menu, self.button_wood,
				(350,60), (0,240), bt4[0], self.font32, (255,255,255), self.menupos)
	def blit_menu(self):
		self.screen.blit(self.menu, self.menupos)

	def refresh_display(self):
		self.blit_background()
		self.blit_menu()
		self.flip()

	def __init__(self, screen, inputm):
		UI.__init__(self, screen, inputm)
		self.load_background('img/background_main-menu.png')
		self.button_wood = pygame.image.load('img/button_wood.png')
