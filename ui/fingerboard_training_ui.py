from ui.ui import *

class FingerboardTrainingUI(UI):
	notes_pos = {
			'Sol3': [0, [(198,504)]],
			'La3': [1, [(410,506)]],
			'Si3': [2, [(592,508)]],
			'Do4': [3, [(674,508)]],
			'Re4': [4, [(828,510), (196,474)]],

			'Mi4': [5, [(410,470)]],
			'Fa4': [6, [(494,470)]],
			'Sol4': [7, [(674,468)]],
			'La4': [8, [(826,466),(196,446)]],

			'Si4': [9, [(409,440)]],
			'Do5': [10, [(494,436)]],
			'Re5': [11, [(672,430)]],
			'Mi5': [12, [(826,424),(196,418)]],

			'Fa5': [13, [(302,412)]],
			'Sol5': [14, [(494,402)]],
			'La5': [15, [(672,390)]],
			'Si5': [16, [(824,382)]]
	}

	def draw_note(self, note, good):
		c = [(100,0,0), (0,100,14)]
		for p in self.notes_pos[note][1]:
			pygame.draw.rect(self.fingerboard, (255,255,255), (p[0]-32,p[1]-13,64,26), 0)
			pygame.draw.rect(self.fingerboard, c[good], (p[0]-30,p[1]-11,60,22), 0)
			text = self.font32.render(note, 1, (255,255,255))
			textpos = text.get_rect()
			textpos.centerx = p[0]
			textpos.centery = p[1]
			self.fingerboard.blit(text, textpos)
		self.blit_fingerboard()
		self.flip()
	def reset_fingerboard(self):
		self.fingerboard = pygame.Surface((1280,720), pygame.SRCALPHA)
	def blit_fingerboard(self):
		self.screen.blit(self.fingerboard, (0,0))

	def blit_score(self, seq, cur):
		self.score = pygame.Surface((690,350), pygame.SRCALPHA)
		for idx, note in enumerate(seq):
			if self.notes_pos[note][0] <= 1:
				pygame.draw.line(self.score, (0,0,0), (151 + 71*idx, 287),
						(220 + 71*idx, 287), 3)
			if self.notes_pos[note][0] <= 3:
				pygame.draw.line(self.score, (0,0,0), (151 + 71*idx, 258),
						(220 + 71*idx, 258), 3)
			if self.notes_pos[note][0] >= 15:
				pygame.draw.line(self.score, (0,0,0), (151 + 71*idx, 84),
						(220 + 71*idx, 84), 3)
			self.score.blit(self.blue_note if idx == cur else self.note,
					(165 + 71*idx, 206 - self.notes_pos[note][0]*14.5))
		self.screen.blit(self.score, (198,0))

	def show_note(self, note):
		self.note_label = pygame.Surface((300,300), pygame.SRCALPHA)
		text = self.font128.render(note, 1, (255,255,255))
		textpos = text.get_rect()
		textpos.centerx = 150
		textpos.centery = 150
		self.note_label.blit(text, textpos)
		self.screen.blit(self.note_label, (980,80))

	def refresh_ui(self, seq, cur_noteid, show_note, cur_note):
		self.blit_background()
		self.blit_fingerboard()
		self.blit_score(seq, cur_noteid)
		if show_note:
			self.show_note(cur_note)
		self.flip()

	def __init__(self, screen, inputm):
		UI.__init__(self, screen, inputm)
		self.load_background('img/background_fingerboard.png')
		self.note = pygame.image.load('img/note.png')
		self.blue_note = pygame.image.load('img/note_blue.png')
