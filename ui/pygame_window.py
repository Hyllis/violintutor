import pygame

class PygameWindow:
	def __init__(self):
		pygame.init()
		self.screen = pygame.display.set_mode((1280,720))
		pygame.display.set_caption('ViolinTutor')

	def get_screen(self):
		return self.screen
