from ui.ui import *

class SightreadingTrainingUI(UI):
	notes_pos = {
			'Sol3': [0, "Sol", []],
			'La3': [1, "La", []],
			'Si3': [2, "Si", []],
			'Do4': [3, "Do", []],
			'Re4': [4, "Re", []],

			'Mi4': [5, "Mi", []],
			'Fa4': [6, "Fa", []],
			'Sol4': [7, "Sol", []],
			'La4': [8, "La", []],

			'Si4': [9, "Si", []],
			'Do5': [10, "Do", []],
			'Re5': [11, "Re", []],
			'Mi5': [12, "Mi", []],

			'Fa5': [13, "Fa", []],
			'Sol5': [14, "Sol", []],
			'La5': [15, "La", []],
			'Si5': [16, "Si", []]
	}

	def refresh_ui(self, seq, cur_noteid):
		self.blit_background()
		self.blit_score(seq, cur_noteid)
		self.flip()

	def reset_failed_notes(self):
		self.failednotes = pygame.Surface((1280,720), pygame.SRCALPHA)
	def failed_note(self, noteid):
		pygame.draw.rect(self.failednotes, (100,0,0,255), (1073,62 + 88*noteid,149,68), 5)
		self.screen.blit(self.failednotes, (0,0))
		self.flip()

	def blit_score(self, seq, cur):
		self.score = pygame.Surface((1030,720), pygame.SRCALPHA)
		for idx, note in enumerate(seq):
			if self.notes_pos[note][0] <= 1:
				pygame.draw.line(self.score, (0,0,0), (275 + 100*idx, 535),
						(365 + 100*idx, 535), 4)
			if self.notes_pos[note][0] <= 3:
				pygame.draw.line(self.score, (0,0,0), (275 + 100*idx, 491),
						(365 + 100*idx, 491), 4)
			if self.notes_pos[note][0] >= 15:
				pygame.draw.line(self.score, (0,0,0), (275 + 100*idx, 227),
						(365 + 100*idx, 227), 4)
			self.score.blit(self.blue_note if idx == cur else self.note,
					(286 + 101*idx, 410 - self.notes_pos[note][0]*22))
		self.screen.blit(self.score, (0,0))



	def __init__(self, screen, inputm):
		UI.__init__(self, screen, inputm)
		self.reset_failed_notes()
		self.load_background('img/background_sightreading.png')
		self.note = pygame.image.load('img/note_big.png')
		self.blue_note = pygame.image.load('img/note_blue_big.png')
