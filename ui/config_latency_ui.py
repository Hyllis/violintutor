from ui.ui import *

class ConfigLatencyUI(UI):
	def blit_timer(self, cur_time, delay, last_precision, progress):
		self.timer = pygame.Surface((496,336), pygame.SRCALPHA)
		x = 245 + (40 * (cur_time - 0.5))
		pygame.draw.rect(self.timer, (160,160,160,220), (x,48,2,40), 0)

		text = self.font32.render("Delay: "+str(delay)+"ms", 1, (50,50,50))
		textpos = text.get_rect()
		textpos.centerx = 496/4
		textpos.centery = 110
		self.timer.blit(text, textpos)

		text = self.font32.render("Accuracy: "+("+" if last_precision>=0 else "")+str(last_precision)+"ms", 1, (50,50,50))
		textpos = text.get_rect()
		textpos.centerx = 496/4*3
		textpos.centery = 110
		self.timer.blit(text, textpos)

		text = self.font24.render(str(progress)+"/15", 1, (50,50,50))
		textpos = text.get_rect()
		textpos.centerx = 496/2
		textpos.centery = 145
		self.timer.blit(text, textpos)

		self.screen.blit(self.timer, (405,349))
	def refresh_ui(self, cur_time, delay, last_precision, progress):
		self.blit_background()
		self.blit_timer(cur_time, delay, last_precision, progress)
		self.flip()

	def __init__(self, screen, inputm):
		UI.__init__(self, screen, inputm)
		self.load_background('img/background_config.png')
