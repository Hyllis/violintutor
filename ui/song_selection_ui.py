from ui.ui import *

class SongSelectionUI(UI):
	def load_songlist(self, lst):
		self.songlist = pygame.Surface((1280,720), pygame.SRCALPHA)
		sy = 720/2 - (5*10+42*13)/2
		for idx,item in enumerate(lst):
			if item is not None:
				pygame.draw.rect(self.songlist, (46,138,208,128) if idx==6 else (0,0,0,128), (335,sy+48*idx,610,42), 0)
				text = self.font32.render(item, 1, (230,230,230))
				textpos = text.get_rect()
				textpos.centerx = 640
				textpos.centery = sy+48*idx+21
				self.songlist.blit(text, textpos)
	def blit_songlist(self):
		self.screen.blit(self.songlist, (0,0))

	def blit_error(self, t):
		text = self.font32.render(t, 1, (178,40,40))
		textpos = text.get_rect()
		textpos.centerx = 640
		textpos.centery = 40
		self.songlist.blit(text, textpos)


	def refresh_ui(self, lst, e=None):
		self.blit_background()
		self.load_songlist(lst)
		if e is not None: self.blit_error(e)
		self.blit_songlist()
		self.flip()

	def __init__(self, screen, inputm):
		UI.__init__(self, screen, inputm)
		self.load_background('img/background_song-selection.png')
