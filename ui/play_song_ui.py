from ui.ui import *

class PlaySongUI(UI):
	notes_lines = {'0': 339, 'L1': 408, '1': 518, 'L2': 608, '2': 698, '3': 780, 'L4': 860,
			'4': 932, 'H4': 996, '5': 1056, '6': 1100}

	def reset_notes(self):
		self.notes = pygame.Surface((1280,510), pygame.SRCALPHA)
	def blit_notes(self):
		self.screen.blit(self.notes, (0,0))

	def draw_ui(self, infos):
		c = (50,50,50)
		text = self.font68.render(str(infos['score']), 1, c)
		self.notes.blit(text, (15,10))
		t = str(infos['succeed']) + "/" + str(infos['total']) + " (" + str("0" if infos['total']==0 else int(infos['succeed']/float(infos['total'])*100)) + "%)"
		text = self.font32.render(t, 1, c)
		self.notes.blit(text, (18,60))
		text = self.font32.render("Streak: " + str(infos['streak']), 1, c)
		self.notes.blit(text, (18,94))
		text = self.font32.render("Speed: " + infos['speed'], 1, c)
		self.notes.blit(text, (18,118))

		c = (10,74,162)
		text = self.font32.render(infos['last_note'], 1, c)
		textpos = text.get_rect()
		textpos.centerx = 650
		textpos.centery = 60
		self.notes.blit(text, textpos)
		t = infos['last_grading']
		text = self.font54.render(t, 1, c)
		textpos = text.get_rect()
		textpos.centerx = 650
		textpos.centery = 90
		self.notes.blit(text, textpos)
		if t != "FAILED" and t != "":
			t = "(" + ("+" if infos['last_delay']>=0 else "") + str(infos['last_delay']) + "ms)"
			text = self.font24.render(t, 1, c)
			textpos = text.get_rect()
			textpos.centerx = 650
			textpos.centery = 116
			self.notes.blit(text, textpos)

	def draw_note(self, relative_t, note, speed):
		self.notes.blit(self.notes_color[note[0]+("0" if note[1] == "0" else "")],
				(self.notes_lines[note[1]]-35, 480+400*relative_t-20))
	def draw_chunk(self, chunk, notes_pos, cur_t, speed):
		self.reset_notes()
		for t in chunk:
			if len(t)>3: continue
			for n in t[1]:
				if n[0] in notes_pos:
					self.draw_note(cur_t - t[0], notes_pos[n[0]], speed)

	def refresh_display(self):
		self.blit_background()
		self.blit_notes()
		self.flip()

	def __init__(self, screen, inputm):
		UI.__init__(self, screen, inputm)
		self.notes_color = {}
		self.load_background('img/background_play-song.png')
		self.notes_color['G'] = pygame.image.load('img/string_g.png')
		self.notes_color['D'] = pygame.image.load('img/string_d.png')
		self.notes_color['A'] = pygame.image.load('img/string_a.png')
		self.notes_color['E'] = pygame.image.load('img/string_e.png')
		self.notes_color['G0'] = pygame.image.load('img/open_g.png')
		self.notes_color['D0'] = pygame.image.load('img/open_d.png')
		self.notes_color['A0'] = pygame.image.load('img/open_a.png')
		self.notes_color['E0'] = pygame.image.load('img/open_e.png')
		self.notes_color['beat0'] = pygame.image.load('img/beat.png')
		self.notes_color['BEAT0'] = pygame.image.load('img/beatx.png')
		self.reset_notes()
