import pygame

class InputManager:

	def __init__(self, screen):
		print("Init InputManager")
		self.exit = False
		self.screen = screen
		self.mouse_ev = []
		self.keyboard_ev = []

	def wait_event(self):
		print("IN")
		for event in pygame.event.get():
			print("LOOp")
			if event.type == pygame.KEYDOWN:
				print("KD")
				for ev in self.keyboard_ev:
					if ev[0] == event.key:
						ev[1]()
			elif event.type == pygame.MOUSEBUTTONUP:
				p = pygame.mouse.get_pos()
				print("Pos: ", p)
				for ev in self.mouse_ev:
					print("LOOp EV")
					sqr = ev[0]
					if (p[0] > sqr[0] and p[0] < sqr[2]
							and p[1] > sqr[1] and p[1] < sqr[3]):
						ev[1]()

	def add_mouse_event(self, callback, pos):
		if callback is None:
			return
		self.mouse_ev.append((pos, callback))

	def add_keyboard_event(self, callback, key):
		if callback is None:
			return
		self.keyboard_ev.append((key, callback))

