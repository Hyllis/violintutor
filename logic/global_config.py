import xml.etree.ElementTree as ET

class GlobalConfig:
	def load_config(self):
		r = ET.parse(".config").getroot()
		self.latency = float(r.find('latency').text)
		self.audio_loop = r.find('audio_loop').text == "True"
	def save_config(self):
		pass

	def load_default(self):
		self.latency = 0.01
		self.audio_loop = False

	def __init__(self, filename):
		try:
			self.load_config()
		except:
			self.load_default()
