from ui.config_latency_ui import *
from logic.mic_frequency import *
from queue import Queue
import threading
import time

class ConfigLatency:
	def worker(self):
		while not self.exit:
			freq_info = self.mf.get_freq()
			if freq_info[3] > 500 and freq_info[4] and freq_info[0] > 195:
				print(freq_info)
				self.q.put(freq_info)

	def run(self):
		threading.Thread(target=self.worker).start()
		start = time.time()
		f = 0
		beat = strum = False
		delay_sum = []
		delay = self.conf.latency

		while len(delay_sum)<15 and not self.exit:
			self.inputm.check_event()
			cur = time.time() - start
			if cur > 0.48 and cur < 0.52 and not beat:
				beat = True
			elif cur > 0.9999:
				with self.q.mutex:
					self.q.queue.clear()
				strum = beat = False
				f = 0
				start = time.time()
			if f % 1000 == 0:
				self.ui.refresh_ui(cur, int(delay*1000), self.last_precision, len(delay_sum))

			if not self.q.empty() and not strum:
				e = self.q.get_nowait()
				strum = True

				d = 0.5-(e[5]-start)
				delay_sum.append(d)
				delay = sum(delay_sum)/len(delay_sum)

				self.last_precision = int((d-delay)*1000)

			f += 1
		self.conf.latency = delay
		print("Final delay: " + str(int(delay*1000)))


	def callback_key_escape(self, args):
		self.exit = True

	def __init__(self, screen, c):
		self.exit = False
		self.screen = screen
		self.conf = c
		self.cur_time = 0.0
		self.last_precision = 0
		self.inputm = InputManager(screen)
		self.inputm.add_keyboard_event(self.callback_key_escape, pygame.K_ESCAPE)
		self.ui = ConfigLatencyUI(screen, self.inputm)
		self.ui.refresh_ui(self.cur_time, self.conf.latency, self.last_precision, 0)
		self.mf = MicFrequency(self.conf)
		self.q = Queue()
