import xml.etree.ElementTree as ET
import time

class MusicXMLParser:
	note_translation = {'C': 'Do', 'D': 'Re', 'E': 'Mi', 'F': 'Fa',
			'G': 'Sol', 'A': 'La', 'B': 'Si'}
	note_types = ['whole', 'half', 'quarter', 'eighth', '16th', '32nd', '64th']
	note_type = {'whole': 1, 'half': 0.5, 'quarter': 0.25, 'eighth': 0.125,
			'16th': 1/16.0, '32nd': 1/32.0, '64th': 1/64.0}

	def __init__(self, filename):
		self.r = ET.parse(filename).getroot()
		self.cur_measure = 0
		self.cur_note = 0
		self.notes_chunk = []

	def get_parts(self):
		if self.r.tag != 'score-partwise':
			raise Exception(["Unsupported MusicXML format","'score-partwise' not found"])
		partlist = self.r.find('part-list')
		if partlist is None:
			raise Exception("Unsupported MusicXML format","'partlist' not found")
		parts = []
		for p in partlist.findall('score-part'):
			n = (p.attrib['id'], p.find('part-name').text)
			parts.append(n)
		return parts

	def select_part(self, part):
		self.part = None
		self.measures = []
		for p in self.r.findall('part'):
			if p.get('id') == part[0]:
				self.part = p
				break
		if self.part is None:
			raise Exception(["Unsupported MusicXML format","'part' not found"])
		self.parse_part(self.part)

	def parse_part(self, part):
		mconf = {'metronome_bpm': 80, 'timesig_beats': 4, 'timesig_beattype': 4,
				'duration': 0.0, 'bar_stack': [], 'left_bars': [], 'right_bars': []}
		idx = 0
		while idx < len(part.findall('measure')):
			m = part.findall('measure')[idx]
			for bl in m.findall('barline'):
				if bl.find('repeat') is not None:
					if bl.get('location') == "left" and idx not in mconf['left_bars']:
						mconf['left_bars'].insert(0, idx)
						mconf['bar_stack'].insert(0, idx)
					if bl.get('location') == "right" and idx not in mconf['right_bars']:
						mconf['bar_stack'].append(0)
						mconf['right_bars'].insert(0, idx)
						idx = mconf['bar_stack'].pop(0)-1
			self.parse_attrib_dir(m, mconf)
			self.measures.append(self.parse_measure(m, mconf))
			idx += 1

	def parse_attrib_dir(self, m, mconf):
		attrib = m.find('attributes')
		if attrib is not None:
			if attrib.find('divisions') is not None:
				mconf['divisions'] = attrib.find('divisions').text
			if attrib.find('key') is not None and attrib.find('key').find('fifths') is not None:
				mconf['key_fifths'] = attrib.find('key').find('fifths').text
			if attrib.find('time') is not None:
				mconf['timesig_beats'] = attrib.find('time').find('beats').text
				mconf['timesig_beattype'] = attrib.find('time').find('beat-type').text
			if attrib.find('clef') is not None:
				mconf['clef_sign'] = attrib.find('clef').find('sign').text
				mconf['clef_line'] = attrib.find('clef').find('line').text
		dirct = m.find('direction')
		if dirct is not None:
			mconf['direction_placement'] = dirct.get('placement')
			if (dirct.find('direction-type') is not None
					and dirct.find('direction-type').find('metronome') is not None):
				metronome = dirct.find('direction-type').find('metronome')
				if metronome.find('beat-unit') is not None:
					mconf['metronome_beatunit'] = metronome.find('beat-unit').text
				if metronome.find('per-minute') is not None:
					mconf['metronome_bpm'] = metronome.find('per-minute').text

	def get_note_type_value(self, note):
		tm = note.find('time-modification')
		time_mod = 1.0
		total_type = 0
		if tm is not None:
			time_mod = float(tm.find('normal-notes').text) / float(tm.find('actual-notes').text)

		dotcount = len(note.findall('dot'))
		if dotcount >= 1:
			typeid = int(self.note_types.index(note.find('type').text))
			total_type += self.note_type[self.note_types[typeid + 1]] * time_mod
			if dotcount >= 2:
				total_type += self.note_type[self.note_types[typeid + 2]] * time_mod
		total_type += self.note_type[note.find('type').text] * time_mod
		return total_type

	def get_measure_duration(self, m):
		total = 0
		total_type = 0.0
		for note in m.findall('note'):
			if note.find('chord') is None and note.find('duration') is not None:
				total += int(note.find('duration').text)
			if note.find('type') is not None:
				total_type += self.get_note_type_value(note)
		return (total,total_type)

	def parse_measure(self, m, mconf):
		dur,durtype = self.get_measure_duration(m)
		mn = [None]*dur*2
		ret = {'number': m.get('number'), 'dur': dur, 'notes': mn,
				'beats': int(mconf['timesig_beats']), 'songpos': mconf['duration'],
				'beattype': int(mconf['timesig_beattype'])}

		dur_calc = 1.0/ret['beattype']*ret['beats']
		if durtype == 0: durtype = dur_calc
		dur_diff = durtype - dur_calc
		if dur_diff != 0:
			ret['beats'] = ret['beats'] + (dur_diff / (dur_calc / ret['beattype']))

		ret['bpm'] = int(mconf['metronome_bpm']) * (0.25 * ret['beattype'])
		ret['dur_s'] = 60.0 / ret['bpm'] * ret['beats']
		ret['inter'] = ret['dur_s'] / (ret['dur'] if ret['dur']>0 else 1)
		mconf['duration'] += ret['dur_s']

		i = 0
		# Add beats
		while i < ret['dur']:
			if mn[i] is None: mn[i] = []
			mn[i].append(["BEAT" if i == 0 else "beat", -1])
			i += int(ret['dur'] / ret['beats'])
			if i<1: break

		i = 0
		# Add notes
		for note in m.findall('note'):
			if note.find('pitch') is not None:
				notelabel = self.note_translation[note.find('pitch').find('step').text]
				alt = note.find('pitch').find('alter')
				if alt is not None:
					if alt.text == '1': notelabel += "#"
					elif alt.text == '-1': notelabel += 'b'
				notelabel += note.find('pitch').find('octave').text
			if note.find('rest') is not None:
				notelabel = "REST"

			if note.find('chord') is not None:
				i -= int(note.find('duration').text)
			if mn[i] is None: mn[i] = []
			if note.find('duration') is not None:
				mn[i].append([notelabel, int(note.find('duration').text)])
				i += int(note.find('duration').text)

		return ret


	def gen_note_chunk(self, start, dur):
		lastm = self.measures[len(self.measures)-1]
		total_duration = lastm['songpos'] + lastm['inter']*lastm['dur'] + 1
		if start > total_duration: return None

		while len(self.notes_chunk) > 0 and self.notes_chunk[0][0] < start:
			self.notes_chunk.pop(0)

		while self.cur_measure < len(self.measures):
			m = self.measures[self.cur_measure]
			while self.cur_note < len(m['notes']):
				n = m['notes'][self.cur_note]
				if n is not None:
					notepos = m['songpos']+m['inter']*self.cur_note
					if notepos > start + dur: return self.notes_chunk
					if notepos > start:
						self.notes_chunk.append([notepos, n])
				self.cur_note += 1
			self.cur_note = 0
			self.cur_measure += 1
		return self.notes_chunk


	def debug_play(self):
		start = time.time()
		for m in self.measures:
			for idx,n in enumerate(m['notes']):
				notepos = m['songpos']+m['inter']*idx
				while time.time()-start < notepos:
					tts = time.time() - start
					if notepos-tts > 0.01:
						time.sleep(notepos-tts-0.005)
					pass
				if n is not None:
					log = "--> " + str(notepos) + "s / "
					for idx,nx in enumerate(n):
						if nx[0] != "REST":
							log += (", " if idx else "") + nx[0]
					print(log)

if __name__ == "__main__":
	ms = MusicXMLParser('songs/Princess_Mononoke-Theme.mxl')
	parts = ms.get_parts()
	ms.select_part(parts[0])
	ms.debug_play()
#print ms.gen_note_chunk(10.0, 10)
