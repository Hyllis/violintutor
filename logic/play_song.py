from ui.play_song_ui import *
from logic.config_latency import *
import threading
import time

class PlaySong:
	notes_pos = {
			#0
			"Sol3": ["G", "0"],
			"Re4": ["D", "0"],
			"La4": ["A", "0"],
			"Mi5": ["E", "0"],
			"Fab5": ["E", "0"],

			#L1
			"Sol#3": ["G", "L1"],
			"Lab3": ["G", "L1"],
			"Re#4": ["D", "L1"],
			"Mib4": ["D", "L1"],
			"La#4": ["A", "L1"],
			"Sib4": ["A", "L1"],
			"Mi#5": ["E", "L1"],
			"Fa5": ["E", "L1"],

			#1
			"La3": ["G", "1"],
			"Mi4": ["D", "1"],
			"Fab4": ["D", "1"],
			"Si4": ["A", "1"],
			"Dob5": ["A", "1"],
			"Fa#5": ["E", "1"],
			"Solb5": ["E", "1"],

			#L2
			"Sib3": ["G", "L2"],
			"La#3": ["G", "L2"],
			"Mi#4": ["D", "L2"],
			"Fa4": ["D", "L2"],
			"Do5": ["A", "L2"],
			"Si#4": ["A", "L2"],
			"Sol5": ["E", "L2"],

			#2
			"Si3": ["G", "2"],
			"Dob4": ["G", "2"],
			"Fa#4": ["D", "2"],
			"Solb4": ["D", "2"],
			"Do#5": ["A", "2"],
			"Reb5": ["A", "2"],
			"Sol#5": ["E", "2"],
			"Lab5": ["E", "2"],

			#3
			"Do4": ["G", "3"],
			"Si#3": ["G", "3"],
			"Sol4": ["D", "3"],
			"Re5": ["A", "3"],
			"La5": ["E", "3"],

			#L4
			"Do#4": ["G", "L4"],
			"Reb4": ["G", "L4"],
			"Sol#4": ["D", "L4"],
			"Lab4": ["D", "L4"],
			"Mib5": ["A", "L4"],
			"Re#5": ["A", "L4"],
			"Sib5": ["E", "L4"],
			"La#5": ["E", "L4"],

			#4
			#"Re4": ["G", "4"],
			#"La4": ["D", "4"],
			#"Mi5": ["A", "4"],
			"Si5": ["E", "4"],
			"Dob6": ["E", "4"],

			"Do6": ["E", "H4"],
			"Do#6": ["E", "5"],
			"Re6": ["E", "6"],

			"beat": ["beat", "0"],
			"BEAT": ["BEAT", "0"]
	}

	def worker(self):
		while not self.exit:
			freq_info = self.mf.get_freq()
			if freq_info is None:
				continue
			if freq_info[3] > 500 and freq_info[4] and freq_info[0] > 195:
				self.q.put(freq_info)
				print("Got: " + freq_info[1])

	def callback_config_latency(self, args):
		conf = ConfigLatency(self.screen, self.conf)
		conf.run()
		self.ui.refresh_display()
	def callback_key_escape(self, args):
		self.exit = True
	def callback_tempo_down(self, args):
		if self.speed > 0.1:
			self.speed -= 0.1
		self.infos['speed'] = str(int(self.speed*100)) + "%"
		print(self.speed)
	def callback_tempo_up(self, args):
		self.speed += 0.1
		self.infos['speed'] = str(int(self.speed*100)) + "%"
		print(self.speed)

	def __init__(self, screen, c, ms):
		self.exit = False
		self.screen = screen
		self.conf = c
		self.ms = ms
		self.inputm = InputManager(screen)
		self.inputm.add_keyboard_event(self.callback_key_escape, pygame.K_ESCAPE)
		self.inputm.add_keyboard_event(self.callback_config_latency, pygame.K_c)
		self.inputm.add_keyboard_event(self.callback_tempo_down, pygame.K_LEFT)
		self.inputm.add_keyboard_event(self.callback_tempo_up, pygame.K_RIGHT)
		self.ui = PlaySongUI(screen, self.inputm)
		self.speed = 1.0
		self.next_notes = []
		self.mf = MicFrequency(self.conf)
		self.q = Queue()
		self.note_delays = []
		self.failed = 0
		self.infos = {'succeed': 0, 'failed': 0, 'total': 0, 'score': 0, 'streak': 0,
				'last_note': "", 'last_grading': "", 'last_delay': 0, 'speed': str(int(self.speed*100))+"%"}
		self.ui.refresh_display()

	def update_next_notes(self, chunk):
		for idx,n in enumerate(self.next_notes):
			if abs(n[0]-self.cur)>0.3+(self.conf.latency if self.conf.latency > 0 else 0):
				print("Failed: " + n[1])
				self.infos['failed'] += 1
				self.infos['total'] += 1
				self.infos['last_note'] = n[1]
				self.infos['last_grading'] = "FAILED"
				self.infos['streak'] = 0
				self.next_notes.pop(idx)
		if chunk is None: return
		for t in chunk:
			if abs(t[0]-self.cur)>0.2: break
			if len(t)>2: continue
			for n in t[1]:
				if n[0] != "beat" and n[0] != "BEAT" and n[0] != "REST":
					self.next_notes.append([t[0],n[0],t])
					t.append("treated")

	def set_grading(self, d):
		if d>180: return "VERY SLOW"
		if d>80:  return "SLOW"
		if d<-180: return "WAY TOO FAST"
		if d<-80:  return "TOO FAST"
		if -15<d<15: return "PERFECT"
		return "OKAY"
	def play_note(self, e):
		t = (e[5] - self.start) * self.speed
		for idx,n in enumerate(self.next_notes):
			if n[1] == e[1]:
				self.note_delays.append(int((n[0]-t-self.conf.latency)*1000))
				self.infos['last_delay'] = int((t-n[0]-self.conf.latency)*1000)
				self.infos['last_note'] = n[1]
				self.infos['last_grading'] = self.set_grading(self.infos['last_delay'])
				self.infos['streak'] += 1
				self.infos['succeed'] += 1
				self.infos['total'] += 1
				self.infos['score'] += (300-abs(self.infos['last_delay'])) * int(1+min(100, self.infos['streak']) / 20.0);
				print("Played: " + n[1] + " - " + str(self.infos['last_delay']) + "ms")
				n[2].append("played")
				self.next_notes.pop(idx)
				break

	def run(self):
		t = threading.Thread(target=self.worker)
		t.start()
		self.start = time.time() + 3
		f = 0
		while not self.exit:
			self.cur = (time.time() - self.start) * self.speed
			chunk = self.ms.gen_note_chunk(self.cur, 1.5 / self.speed * 2)
			#print(chunk)
			self.update_next_notes(chunk)
			if not self.q.empty():
				e = self.q.get_nowait()
				self.play_note(e)
			if chunk is None:
				print("Song finished!")
				break
			if f % 100 == 0:
				self.ui.draw_chunk(chunk, self.notes_pos, self.cur, self.speed)
				self.ui.draw_ui(self.infos)
				self.ui.refresh_display()
			self.inputm.check_event()
			f += 1
