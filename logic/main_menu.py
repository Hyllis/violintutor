from ui.main_menu_ui import *
from logic.song_selection import *
from logic.fingerboard_training import *
from logic.sightreading_training import *

class MainMenu:
	def callback_sightreading(self, args):
		sr = SightreadingTraining(self.screen, self.conf)
		sr.run()
		self.ui.refresh_display()
	def callback_fingerboard(self, args):
		fb = FingerboardTraining(self.screen, self.conf)
		fb.run()
		self.ui.refresh_display()
	def callback_play(self, args):
		ss = SongSelection(self.screen, self.conf)
		ss.run()
		self.ui.refresh_display()
	def callback_key_escape(self, args):
		self.exit = True

	def __init__(self, screen, c):
		self.exit = False
		self.screen = screen
		self.conf = c
		self.inputm = InputManager(screen)
		self.inputm.add_keyboard_event(self.callback_key_escape, pygame.K_ESCAPE)
		self.inputm.add_keyboard_event(self.callback_sightreading, pygame.K_s)
		self.inputm.add_keyboard_event(self.callback_fingerboard, pygame.K_f)
		self.inputm.add_keyboard_event(self.callback_play, pygame.K_p)
		self.inputm.add_keyboard_event(self.callback_key_escape, pygame.K_e)
		self.ui = MainMenuUI(screen, self.inputm)
		self.ui.load_menu(("(S)ight-reading training", self.callback_sightreading),
				("(F)ingerboard training", self.callback_fingerboard),
				("(P)lay a song", self.callback_play),
				("(E)xit", self.callback_key_escape))
		self.ui.refresh_display()

	def run(self):
		while not self.exit:
			self.inputm.check_event()
