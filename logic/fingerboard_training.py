from ui.fingerboard_training_ui import *
from logic.mic_frequency import *
from queue import Queue
import random
import threading
import time

class FingerboardTraining:
	notes = ['Sol3', 'La3', 'Si3', 'Do4', 'Re4', 'Mi4', 'Fa4', 'Sol4',
			'La4', 'Si4', 'Do5', 'Re5', 'Mi5', 'Fa5', 'Sol5', 'La5', 'Si5']#, 'Do6']
	notes_extended = notes + ['Sol#4', 'La#3', 'Do#4', 'Re#4', 'Fa#4',
			'Sol#4', 'La#4', 'Do#5', 'Re#5', 'Fa#5', 'Sol#5', 'La#5']

	def callback_toggle_show_note(self, args):
		if ('optional' in args):
			print("YAYAYAY",args['optional'])
		self.show_note = False if self.show_note else True
		self.ui.refresh_ui(self.seq, self.cur_noteid, self.show_note, self.seq[self.cur_noteid])
	def callback_key_escape(self, args):
		self.exit = True

	def next_note(self):
		self.ui.reset_fingerboard()
		self.cur_noteid += 1
		if self.cur_noteid >= 7:
			self.gen_notes()
		self.ui.refresh_ui(self.seq, self.cur_noteid, self.show_note, self.seq[self.cur_noteid])

	def gen_notes(self):
		self.seq = []
		self.cur_noteid = 0
		i = 0
		while i < 7:
			self.seq.append(random.choice(self.notes))
			i += 1

	def __init__(self, screen, c):
		self.exit = False
		self.conf = c
		self.show_note = True
		self.note_found = False
		self.inputm = InputManager(screen)
		self.inputm.add_keyboard_event(self.callback_key_escape, pygame.K_ESCAPE)
		self.inputm.add_keyboard_event(self.callback_toggle_show_note, pygame.K_t)
		self.ui = FingerboardTrainingUI(screen, self.inputm)

		self.ui.reset_fingerboard()
		self.gen_notes()

		self.ui.flip()
		self.ui.refresh_ui(self.seq, self.cur_noteid, self.show_note, self.seq[self.cur_noteid])

	def worker(self):
		while not self.exit:
			freq_info = self.mf.get_freq()
			if freq_info is None:
				continue
			if self.note_found:
				time.sleep(0.5)
				self.note_found = False
				self.next_note()
			if freq_info[0] > 180 and freq_info[3] > 500:
				self.q.put(freq_info)

	def run(self):
		self.mf = MicFrequency(self.conf)
		self.q = Queue()
		threading.Thread(target=self.worker).start()

		while not self.exit:
			self.inputm.check_event()
			if self.note_found:
				with self.q.mutex:
					self.q.queue.clear()
				continue
			try:
				e = self.q.get(True, 0.2)
				if e[1] == self.seq[self.cur_noteid]:
					self.ui.draw_note(e[1], True)
					self.note_found = True
				else:
					self.ui.draw_note(e[1], False)
				print(e)
			except:
				pass
