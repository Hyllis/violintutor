from ui.song_selection_ui import *
from logic.play_song import *
from logic.musicxml_parser import *
from os import listdir
import signal

class SongSelection:
	def run(self):
		while not self.exit:
			self.inputm.check_event()

	def sig_handler(signum, frame):
		raise Exception("Parsing timeout")

	def callback_key_escape(self, args):
		self.exit = True
	def callback_prev_item(self, args):
		if self.lst[5] is not None:
			self.cur_item -= 1
		self.build_sublist()
		self.ui.refresh_ui(self.lst)
	def callback_next_item(self, args):
		if self.lst[7] is not None:
			self.cur_item += 1
		self.build_sublist()
		self.ui.refresh_ui(self.lst)
	def callback_select_item(self, args):
		try:
			item = self.lst[6]
			if item is not None:
				if self.select_part:
					signal.alarm(15)
					self.ms.select_part(self.partlist[self.cur_item+13])
					signal.alarm(0)
					ps = PlaySong(self.screen, self.conf, self.ms)
					ps.run()
					self.select_part = False
					self.cur_item = 0
					self.build_sublist()
					self.ui.refresh_ui(self.lst)
				else:
					signal.alarm(10)
					self.ms = MusicXMLParser("songs/" + item)
					self.partlist = ([None]*13) + self.ms.get_parts() + ([None]*13)
					signal.alarm(0)
					self.select_part = True
					self.cur_item = 0
					self.build_sublist()
					self.ui.refresh_ui(self.lst)
		except Exception as e:
			signal.alarm(0)
			self.select_part = False
			self.cur_item = 0
			self.build_sublist()
			self.ui.refresh_ui(self.lst, str(e))

	def fetch_songs(self):
		try:
			self.song_folder = ([None]*13) + listdir("songs") + ([None]*13)
			self.cur_item = int((len(self.song_folder)-26)/2)
		except: raise Exception("No songs/ folder")
	def build_sublist(self):
		if self.select_part:
			self.lst = self.partlist[self.cur_item+13-6:self.cur_item+13+6]
			for idx,e in enumerate(self.lst):
				if e is not None:
					self.lst[idx] = e[1]
		else: self.lst = self.song_folder[self.cur_item+13-6:self.cur_item+13+6]

	def __init__(self, screen, c):
		self.exit = False
		self.screen = screen
		self.conf = c
		self.inputm = InputManager(screen)
		self.inputm.add_keyboard_event(self.callback_key_escape, pygame.K_ESCAPE)
		self.inputm.add_keyboard_event(self.callback_prev_item, pygame.K_UP)
		self.inputm.add_keyboard_event(self.callback_next_item, pygame.K_DOWN)
		self.inputm.add_keyboard_event(self.callback_select_item, pygame.K_KP_ENTER)
		self.inputm.add_keyboard_event(self.callback_select_item, pygame.K_RETURN)
		self.select_part = False
		signal.signal(signal.SIGALRM, self.sig_handler)
		self.ui = SongSelectionUI(screen, self.inputm)
		self.fetch_songs()
		self.build_sublist()
		self.ui.refresh_ui(self.lst)
