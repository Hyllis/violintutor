from ui.sightreading_training_ui import *
import random

class SightreadingTraining:
	notes = ['Sol3', 'La3', 'Si3', 'Do4', 'Re4', 'Mi4', 'Fa4', 'Sol4',
			'La4', 'Si4', 'Do5', 'Re5', 'Mi5', 'Fa5', 'Sol5', 'La5', 'Si5']#, 'Do6']
	notes_short = "Do Re Mi Fa Sol La Si Do".split()

	def callback_key_escape(self, args):
		self.exit = True
	def callback_select_note(self, args):
		if args['note'] == self.ui.notes_pos[self.seq[self.cur_noteid]][1]:
			self.next_note()
		else:
			self.ui.failed_note(self.notes_short.index(args['note']))

	def next_note(self):
		self.ui.reset_failed_notes()
		self.cur_noteid += 1
		if self.cur_noteid >= 7:
			self.gen_notes()
		self.ui.refresh_ui(self.seq, self.cur_noteid)

	def gen_notes(self):
		self.seq = []
		self.cur_noteid = 0
		i = 0
		while i < 7:
			self.seq.append(random.choice(self.notes))
			i += 1


	def __init__(self, screen, c):
		self.exit = False
		self.screen = screen
		self.conf = c

		self.inputm = InputManager(screen)
		self.inputm.add_keyboard_event(self.callback_key_escape, pygame.K_ESCAPE)
		self.inputm.add_mouse_event(self.callback_select_note, (1071,60,1224,132), note="Do")
		self.inputm.add_mouse_event(self.callback_select_note, (1071,148,1224,220), note="Re")
		self.inputm.add_mouse_event(self.callback_select_note, (1071,236,1224,308), note="Mi")
		self.inputm.add_mouse_event(self.callback_select_note, (1071,324,1224,396), note="Fa")
		self.inputm.add_mouse_event(self.callback_select_note, (1071,412,1224,484), note="Sol")
		self.inputm.add_mouse_event(self.callback_select_note, (1071,500,1224,572), note="La")
		self.inputm.add_mouse_event(self.callback_select_note, (1071,588,1224,660), note="Si")
		self.inputm.add_keyboard_event(self.callback_select_note, pygame.K_c, note="Do")
		self.inputm.add_keyboard_event(self.callback_select_note, pygame.K_d, note="Re")
		self.inputm.add_keyboard_event(self.callback_select_note, pygame.K_e, note="Mi")
		self.inputm.add_keyboard_event(self.callback_select_note, pygame.K_f, note="Fa")
		self.inputm.add_keyboard_event(self.callback_select_note, pygame.K_g, note="Sol")
		self.inputm.add_keyboard_event(self.callback_select_note, pygame.K_a, note="La")
		self.inputm.add_keyboard_event(self.callback_select_note, pygame.K_b, note="Si")

		self.gen_notes()
		self.ui = SightreadingTrainingUI(screen, self.inputm)
		self.ui.refresh_ui(self.seq, self.cur_noteid)

	def run(self):
		while not self.exit:
			self.inputm.check_event()
