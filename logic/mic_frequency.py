import numpy as np
import pyaudio
import audioop
import time
import math

class MicFrequency:
	SAMPLING_RATE = 50400
	FRAME_SIZE = 4096
	BIN_SIZE = float(SAMPLING_RATE)/FRAME_SIZE

	NOTE_NAMES = 'Do Do# Re Re# Mi Fa Fa# Sol Sol# La La# Si'.split()

	# A440 = 69 -- http://subsynth.sourceforge.net/midinote2freq.html
	def freq_to_number(self, f): return 69 + 12*np.log2(f/440.0)
	def number_to_freq(self, n): return 440 * 2.0**((n-69)/12.0)
	def note_name(self, n): return self.NOTE_NAMES[n % 12] + str(math.trunc(n/12 - 1))

	def __init__(self, conf):
		self.conf = conf
		self.d3bin = int(146.83 / self.BIN_SIZE)
		self.c7bin = int(2093.0 / self.BIN_SIZE)
		self.last_volume = 0

		self.stream = pyaudio.PyAudio().open(format=pyaudio.paInt16,
				channels=1,
				rate=self.SAMPLING_RATE,
				input=True,
				output=True,
				frames_per_buffer=self.FRAME_SIZE)
		self.stream.start_stream()
		self.data = None
#		self.mutex = threading.Lock()
#		thread.start_new_thread(self.worker, ())

	def worker(self):
		while True:
			with self.mutex:
				if self.data is not None:
					self.stream.write(self.data)
					self.data = None

	def get_freq(self):
		num_frames = 0
		if self.stream.is_active():
#			with self.mutex:
			self.data = self.stream.read(self.FRAME_SIZE)
			if self.conf.audio_loop:
				self.stream.write(self.data)
			volume = audioop.rms(self.data, 2)

			fft = np.fft.rfft(np.fromstring(self.data, np.int16))
			#Select largest bin between D3 and C7 and turn it back to frequency
			freq = (np.abs(fft[self.d3bin:self.c7bin]).argmax() + self.d3bin) * self.BIN_SIZE

			n = int(round(self.freq_to_number(freq)))

			is_strum = True if volume > self.last_volume + 100 else False
			self.last_volume = volume
			return (freq, self.note_name(n), self.number_to_freq(n)-freq, volume, is_strum, time.time())
		else:
			print("STREAM IS NOT ACTIVE")
		return None

######################################################################
# Based on: https://github.com/mzucker/python-tuner/blob/master/tuner.py
# Author:  Matt Zucker
# Date:    July 2016
# License: Creative Commons Attribution-ShareAlike 3.0
#          https://creativecommons.org/licenses/by-sa/3.0/us/
######################################################################

if __name__ == "__main__":
	mf = MicFrequency()
	start = time.time()
	c = 0
	while True:
		cur = time.time()
		e = mf.get_freq()
		c += 1
		if (cur - start > 1):
			print(": " + str(c) + " - Time: " + str(cur-start))
			c = 0
			start = time.time()
		if e[3]>500 and e[4]:
			print(e)
