from ui.pygame_window import *
from logic.main_menu import *
from logic.global_config import *

w = PygameWindow()
c = GlobalConfig('.config')
menu = MainMenu(w.get_screen(), c)
menu.run()
